import React from "react";

export default class ComponenteUm extends React.Component {
  render() {
    return (
      <h1
        style={{
          color: this.props.color,
        }}
      >
        Primeiro componente
      </h1>
    );
  }
}
