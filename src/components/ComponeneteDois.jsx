import React from "react";

export default class ComponenteDois extends React.Component {
  render() {
    return (
      <h1
        style={{
          fontWeight: this.props.fontWeight,
        }}
      ></h1>
    );
  }
}
