import "./App.css";
import React from "react";
import Border from "./components/Border";
import ComponenteUm from "./components/ComponenteUm";
import ComponenteDois from "./components/ComponeneteDois";

export default function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Border title="Primeiro Teste">
          <ComponenteUm color="Blue" />
        </Border>

        <Border title="Segundo Teste">
          <ComponenteDois fontWeight="bold" />
        </Border>
      </header>
    </div>
  );
}
